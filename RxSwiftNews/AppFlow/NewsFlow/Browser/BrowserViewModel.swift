//
//  BrowserViewModel.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 03.08.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol BrowserViewModelType: AnyObject {
    var startPageURL: URL { get }
    var metaWebScript: String { get }
    
    func getMenuAlertActions() -> [AlertHelper.AlertAction]
    
    func dismiss()
}

class BrowserViewModel: BrowserViewModelType {
    private let coordinator: BrowserCoordinatorType
    private let serviceHolder: ServiceHolder
    private let url: URL
    
    private let disposeBag = DisposeBag()
    
    init?(coordinator: BrowserCoordinatorType, url: URL, serviceHolder: ServiceHolder?) {
        guard let serviceHolder = serviceHolder else { return nil }
        
        self.coordinator = coordinator
        self.serviceHolder = serviceHolder
        self.url = url
    }
    
    var startPageURL: URL { return url }
    
    var metaWebScript: String {
        return """
        var meta = document.createElement('meta');
        meta.name = 'viewport';
        meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';
        var head = document.getElementsByTagName('head')[0];
        head.appendChild(meta);
        """
    }
    
    func getMenuAlertActions() -> [AlertHelper.AlertAction] {
        let openInSafari: PublishSubject<Void> = .init()
        openInSafari.subscribe { [weak self] _ in
            guard let self = self else { return }
            self.coordinator.openInSafari(url: self.url)
        }.disposed(by: self.disposeBag)
        
        let copyLinkToBuffer: PublishSubject<Void> = .init()
        copyLinkToBuffer.subscribe { _ in
            UIPasteboard.general.url = self.url
        }.disposed(by: self.disposeBag)
        
        return [
            .action(title: "Open in Safari", style: .default, action: openInSafari),
            .action(title: "Copy to Buffer", style: .default, action: copyLinkToBuffer),
            .action(title: "Cancle", style: .cancel)
        ]
    }
    
    func dismiss() {
        coordinator.dismiss()
    }
}
