//
//  BrowserCoordinator.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 03.08.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit

protocol BrowserCoordinatorTransitions: AnyObject {
    
}

protocol BrowserCoordinatorType {
    func start()
    func openInSafari(url: URL)
    func dismiss()
}

extension Storyboard {
    static let browser = UIStoryboard(name: "Browser", bundle: nil)
}

class BrowserCoordinator: BrowserCoordinatorType {
    private weak var navigationController: UINavigationController?
    private weak var transitions: BrowserCoordinatorTransitions?
    private weak var viewController = Storyboard.browser.controller(withClass: BrowserViewController.self)
    private let serviceHolder: ServiceHolder?
    
    init(navigationController: UINavigationController?, url: URL, serviceHolder: ServiceHolder?, transitions: BrowserCoordinatorTransitions? = nil) {
        self.navigationController = navigationController
        self.serviceHolder = serviceHolder
        self.transitions = transitions
        
        self.viewController?.viewModel = BrowserViewModel(coordinator: self, url: url, serviceHolder: serviceHolder)
    }
    
    func start() {
        guard let viewController = viewController else { return }
        viewController.modalPresentationStyle = .pageSheet
        viewController.modalTransitionStyle = .coverVertical
        navigationController?.present(viewController, animated: true)
    }
    
    func openInSafari(url: URL) {
        UIApplication.shared.open(url)
    }
    
    func dismiss() {
        navigationController?.dismiss(animated: true)
    }
}
