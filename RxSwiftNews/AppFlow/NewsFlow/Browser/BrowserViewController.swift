//
//  BrowserViewController.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 03.08.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit
import WebKit
import RxSwift
import RxCocoa

class BrowserViewController: UIViewController {
    @IBOutlet private weak var topBar: BrowserTopBar!
    @IBOutlet private weak var webView: WKWebView!
    
    private let disposeBag = DisposeBag()
    
    var viewModel: BrowserViewModelType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadStartPage()
    }
    
    private func setupUI() {
        setupWebView()
        topBar.setupBar(webView: self.webView)
        
        topBar.closeBtnObservable.subscribe { [weak self] _ in
            guard let self = self else { return }
            self.viewModel?.dismiss()
        }.disposed(by: disposeBag)
        
        topBar.menuBtnObservable.subscribe { [weak self] _ in
            guard let self = self, let alertActions = self.viewModel?.getMenuAlertActions() else { return }
            AlertHelper.present(parentViewController: self, style: .actionSheet, actions: alertActions)
        }.disposed(by: disposeBag)
    }
    
    private func setupWebView() {
        webView.navigationDelegate = self
        webView.scrollView.bouncesZoom = false
        webView.scrollView.isDirectionalLockEnabled = true
        webView.configuration.userContentController = setupScriptsInWebView()
        webView.configuration.mediaTypesRequiringUserActionForPlayback = []
        webView.allowsLinkPreview = false
        webView.configuration.allowsInlineMediaPlayback = false
        webView.configuration.allowsAirPlayForMediaPlayback = false
        webView.configuration.allowsPictureInPictureMediaPlayback = false
        webView.configuration.preferences.javaScriptCanOpenWindowsAutomatically = false
    }
    
    private func setupScriptsInWebView() -> WKUserContentController {
        let contentController = WKUserContentController()
        guard let metaScript = viewModel?.metaWebScript else { return WKUserContentController() }
        let userScript = WKUserScript(source: metaScript, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        contentController.addUserScript(userScript)
        return contentController
    }
}

extension BrowserViewController {
    private func loadStartPage() {
        guard let url = viewModel?.startPageURL else { return }
        loadPageURL(url)
    }
    
    private func refreshWebView() {
        guard let url = self.webView.url else { return }
        loadPageURL(url)
    }
    
    private func loadPageURL(_ url: URL) {
        let request = URLRequest(url: url)
        self.webView.load(request)
    }
}

extension BrowserViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
}
