//
//  NewsCoordinator.swift
//  RxSwift
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit

protocol NewsCoordinatorTransitions: AnyObject {
    
}

protocol NewsCoordinatorType {
    func start()
    func dismiss()
    
    func showDetail(articleModel: ArticleModel)
}

extension Storyboard {
    static let news = UIStoryboard(name: "News", bundle: nil)
}

class NewsCoordinator: NewsCoordinatorType {
    private weak var navigationController: UINavigationController?
    private weak var transitions: NewsCoordinatorTransitions?
    private weak var viewController = Storyboard.news.controller(withClass: NewsViewController.self)
    private let serviceHolder: ServiceHolder?
    
    init(navigationController: UINavigationController?, serviceHolder: ServiceHolder?, transitions: NewsCoordinatorTransitions?) {
        self.navigationController = navigationController
        self.serviceHolder = serviceHolder
        self.transitions = transitions
        
        viewController?.viewModel = NewsViewModel(coordinator: self, serviceHolder: serviceHolder)
    }
    
    func start() {
        guard let viewController = viewController else { return }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showDetail(articleModel: ArticleModel) {
        let coordinator = DetailCoordinator(navigationController: navigationController,
                                            serviceHolder: serviceHolder,
                                            articleModel: articleModel,
                                            transitions: nil)
        coordinator.start()
    }
    
    func dismiss() {
        self.navigationController?.popViewController(animated: true)
    }
}
