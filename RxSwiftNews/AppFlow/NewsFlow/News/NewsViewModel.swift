//
//  NewsViewModel.swift
//  RxSwift
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxRelay

protocol NewsViewModelType: AnyObject {
    func getArticles()
    
    func didSelecteNews(articleModel: ArticleModel?)
    
    var articles: BehaviorRelay<[ArticleModel]> { get set }
}

class NewsViewModel: NewsViewModelType {
    private let coordinator: NewsCoordinatorType
    private let serviceHolder: ServiceHolder
    
    var articles: BehaviorRelay<[ArticleModel]> = .init(value: [])
    
    init?(coordinator: NewsCoordinatorType, serviceHolder: ServiceHolder?) {
        guard let serviceHolder = serviceHolder else { return nil }
        
        self.coordinator = coordinator
        self.serviceHolder = serviceHolder
    }
}

extension NewsViewModel {
    func getArticles() {
        self.getArticlesRequest()
    }
    
    private func getArticlesRequest() {
        let request = GetHeadlinesNewsRequest(locale: "ua")
        request.performRequest(responseModel: HeadelinesNewsModel.self) { [weak self] in
            guard let self = self else { return }
            switch $0 {
            case .success(let model):
                guard let model = model as? HeadelinesNewsModel else { return }
                self.getArticlesImage(artilcleModels: model.articles)
                self.articles.accept(model.articles)
            case .error(let message):
                print("\n \(String(describing: self)) \n ERROR - \(message) ")
            }
        }
    }
    
    private func getArticlesImage(artilcleModels: [ArticleModel]) {
        ImageCachingService.shared.loadImages(items: artilcleModels.compactMap { URL(string: $0.imageURL) })
    }
}

extension NewsViewModel {
    func didSelecteNews(articleModel: ArticleModel?) {
        guard let article = articleModel else { return }
        self.coordinator.showDetail(articleModel: article)
    }
}
