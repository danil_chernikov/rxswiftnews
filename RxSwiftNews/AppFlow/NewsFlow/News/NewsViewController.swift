//
//  NewsViewController.swift
//  RxSwift
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxRelay

class NewsViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    private let disposeBag = DisposeBag()
    
    var viewModel: NewsViewModelType!
            
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindRx()
        viewModel.getArticles()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor(hex: 0xF7F7F7)
        self.setupTableView()
    }
    
    private func setupTableView() {
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.registerNib(cellType: NewsCell.self)
    }
    
    func bindRx() {
        self.viewModel.articles.bind(to: tableView.rx.items(cellIdentifier: NewsCell.reuseIdentifier, cellType: NewsCell.self)) { _, model, cell in
            cell.setup(articleModel: model)
        }.disposed(by: disposeBag)
                
        self.tableView.rx.modelSelected(ArticleModel.self).subscribe { [weak self] in
            guard let self = self else { return }
            self.viewModel.didSelecteNews(articleModel: $0.element)
        }.disposed(by: disposeBag)
    }
}
