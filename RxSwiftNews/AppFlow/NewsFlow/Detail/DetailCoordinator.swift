//
//  DetailCoordinator.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit

protocol DetailCoordinatorTransitions: AnyObject {
    
}

protocol DetailCoordinatorType {
    func start()
    func dismiss()
    
    func showSourceArticle(newsURL: URL)
}

extension Storyboard {
    static let detail = UIStoryboard(name: "Detail", bundle: nil)
}

class DetailCoordinator: DetailCoordinatorType {
    private weak var navigationController: UINavigationController?
    private weak var transitions: DetailCoordinatorTransitions?
    private weak var viewController = Storyboard.detail.controller(withClass: DetailViewController.self)
    private let serviceHolder: ServiceHolder?
    
    init(navigationController: UINavigationController?, serviceHolder: ServiceHolder?, articleModel: ArticleModel, transitions: DetailCoordinatorTransitions?) {
        self.navigationController = navigationController
        self.serviceHolder = serviceHolder
        self.transitions = transitions
        
        self.viewController?.viewModel = DetailViewModel(coordinator: self, serviceHolder: serviceHolder, articleModel: articleModel)
    }
    
    func start() {
        guard let viewController = viewController else { return }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showSourceArticle(newsURL: URL) {
        let coordinator = BrowserCoordinator(navigationController: navigationController,
                                             url: newsURL,
                                             serviceHolder: serviceHolder)
        coordinator.start()
    }
    
    func dismiss() {
        self.navigationController?.popViewController(animated: true)
    }
}
