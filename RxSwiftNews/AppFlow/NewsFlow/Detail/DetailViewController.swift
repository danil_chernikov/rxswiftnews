//
//  DetailViewController.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class DetailViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var navigationBar: NavigationBar!
    
    private let disposeBag = DisposeBag()
    
    var viewModel: DetailViewModelType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindRx()
    }
    
    private func setupUI() {
        setupNavigationBar()
        setupTableView()
    }
    
    private func setupNavigationBar() {
        self.navigationBar.setupBar(leftItem: .init(image: UIImage(systemName: "arrowshape.turn.up.left.fill")?.imageWithColor(color1: UIColor(hex: 0xED526A), renderingMode: .alwaysOriginal)))
    }
    
    private func setupTableView() {
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.tableView.registerNib(cellType: DetailImageCell.self)
        self.tableView.registerNib(cellType: DetailTitleCell.self)
        self.tableView.registerNib(cellType: DetailDescriptionCell.self)
        self.tableView.registerNib(cellType: DetailSourceCell.self)
    }
    
    private func bindRx() {
        self.navigationBar.leftButtonObservable.subscribe { [weak self] _ in
            guard let self = self else { return }
            self.viewModel.dismiss()
        }.disposed(by: disposeBag)
        
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String?, DetailBlockModel>>(configureCell: { [weak self] in
            guard let self = self else { return UITableViewCell() }
            switch $3.type {
            case .image(let url):
                guard let cell: DetailImageCell = $1.dequeCell(for: $2) else { return UITableViewCell() }
                cell.setuImage(url: url)
                return cell
            case .title(let text):
                guard let cell: DetailTitleCell = $1.dequeCell(for: $2) else { return UITableViewCell() }
                cell.setupTitle(text: text)
                return cell
            case .description(let text):
                guard let cell: DetailDescriptionCell = $1.dequeCell(for: $2) else { return UITableViewCell() }
                cell.setupDescription(text: text)
                return cell
            case .source:
                guard let cell: DetailSourceCell = $1.dequeCell(for: $2) else { return UITableViewCell() }
                cell.didTapBtnObservable.subscribe { [weak self] _ in
                    guard let self = self else { return }
                    self.viewModel.showSourceArticle()
                }.disposed(by: self.disposeBag)
                return cell
            }
        })
        
        self.viewModel.detailBlockObservable
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
}
