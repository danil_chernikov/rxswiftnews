//
//  DetailViewModel.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

protocol DetailViewModelType: AnyObject {
    var detailBlockObservable: Observable<[SectionModel<String?, DetailBlockModel>]> { get }

    func showSourceArticle()
    func dismiss()
}

class DetailViewModel: DetailViewModelType {
    private let coordinator: DetailCoordinatorType
    private let serviceHolder: ServiceHolder
    private let articleModel: ArticleModel
    
    private let disposeBag = DisposeBag()
    
    var detailBlockObservable: Observable<[SectionModel<String?, DetailBlockModel>]> = .empty()
    var detailTapOnMoreInfo: Observable<Void> = .empty()
        
    init?(coordinator: DetailCoordinatorType, serviceHolder: ServiceHolder?, articleModel: ArticleModel) {
        guard let serviceHolder = serviceHolder else { return nil }
        
        self.coordinator = coordinator
        self.serviceHolder = serviceHolder
        self.articleModel = articleModel
        
        self.detailBlockObservable = creatingDetailBlocks()
    }
    
    private func creatingDetailBlocks() -> Observable<[SectionModel<String?, DetailBlockModel>]>  {
        let imageBlock = DetailBlockModel(type: .image(URL(string: articleModel.imageURL)))
        let titleBlock = DetailBlockModel(type: .title(articleModel.title ?? ""))
        let descriptionBlock = DetailBlockModel(type: .description(articleModel.description ?? ""))
        let sourceBlock = DetailBlockModel(type: .source)
        return .just([
            .init(model: nil,
                  items: [imageBlock, titleBlock, descriptionBlock, sourceBlock])
        ])
    }
    
    func showSourceArticle() {
        guard let sourceArticleStr = articleModel.url,
            let sourceArticleURL = URL(string: sourceArticleStr) else { return }
        self.coordinator.showSourceArticle(newsURL: sourceArticleURL)
    }
    
    func dismiss() {
        self.coordinator.dismiss()
    }
}
