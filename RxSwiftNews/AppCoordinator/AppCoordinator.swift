//
//  AppCoordinator.swift
//  RxSwift
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit

class AppCoordinator {
    private let window: UIWindow
    private let serviceHolder = ServiceHolder()
    private let navigationController = UINavigationController()
    
    init(window: UIWindow) {
        self.window = window
        
        navigationController.setNavigationBarHidden(true, animated: false)
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        startNews()
    }
    
    private func startNews() {
        let coordinator = NewsCoordinator(navigationController: navigationController,
                                          serviceHolder: serviceHolder,
                                          transitions: nil)
        coordinator.start()
    }
}
