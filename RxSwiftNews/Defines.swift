//
//  Defines.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import Foundation

enum Defines {
    static let keyAPI = "30b0194abd1e4074adb5372a3cd4b31c"
    static let baseURL = "https://newsapi.org/v2"
}
