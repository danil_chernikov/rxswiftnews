//
//  ArticleModel.swift
//  Newsdrop
//
//  Created by Danil Chernikov on 05.02.2020.
//  Copyright © 2020 Onix-Systems. All rights reserved.
//

struct ArticleModel: Codable {
    let source: NewsSourceModel
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let imageURL: String
    let date: String?
    let content: String?
    
    enum CodingKeys: String, CodingKey {
        case source
        case author
        case title
        case description
        case url
        case imageURL = "urlToImage"
        case date = "publishedAt"
        case content
    }
}
