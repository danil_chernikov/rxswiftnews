//
//  HeadelinesNewsModel.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

class HeadelinesNewsModel: Codable {
    let status: String
    let totalResults: Int?
    let articles: [ArticleModel]
}
