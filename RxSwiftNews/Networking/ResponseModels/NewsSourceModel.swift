//
//  NewsSourceModel.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

class NewsSourceModel: Codable {
    let id: String?
    let name: String
}
