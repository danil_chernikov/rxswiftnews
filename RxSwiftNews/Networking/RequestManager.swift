//
//  RequestManager.swift
//  Newsdrop
//
//  Created by Serhii Kobzin on 05.02.2020.
//  Copyright © 2020 Onix-Systems. All rights reserved.
//

import Alamofire

private let jsonDecoder = JSONDecoder()

enum RequestManagerResponse {
    case success(model: Any?)
    case error(message: String)
}

protocol AuthenticationProvider {
    func getToken(completion: @escaping (String?) -> Void)
}

protocol RequestManagerProtocol: class {
    func performRequest<T: Decodable>(_ request: RequestProtocol, responseModel: T.Type, completion: @escaping (RequestManagerResponse) -> Void)
}

class RequestManager: RequestManagerProtocol {
    static let shared = RequestManager()
    
    init() {
        let manager = Alamofire.Session.default
        manager.session.configuration.timeoutIntervalForRequest = 60.0
        
        jsonDecoder.dateDecodingStrategy = .formatted(Date.iso8601Full)
    }
    
    func performRequest<T: Decodable>(
        _ request: RequestProtocol,
        responseModel: T.Type,
        completion: @escaping (RequestManagerResponse) -> Void
    ) {
        _performRequest(request, responseModel: responseModel, completion: completion)
    }
    
    private func _performRequest<T: Decodable>(
        _ request: RequestProtocol,
        responseModel: T.Type,
        token: String? = nil,
        completion: @escaping (RequestManagerResponse) -> Void
    ) {
        if let mockDataFilename = request.mockDataFilename {
            returnDataFromMockup(mockDataFilename: mockDataFilename, responseModel: responseModel, completion: completion)
            return
        }
        let endpoint = request.endpoint
        let httpMethod = request.httpMethod
        let parameters = request.parameters ?? [:]
        var headers = request.headers ?? [:]
        headers["Content-Type"] = "application/json"
        let encoding = request.encoding
        guard let urlString = endpoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString),
            let urlRequest = try? URLRequest(url: url, method: httpMethod), var encodedRequest = try? encoding.encode(urlRequest, with: parameters) else {
                completion(.error(message: "Request Error"))
                return
        }
        if let jsonData = request.rawData {
            encodedRequest.httpBody = jsonData
        }
        let alamofireRequest: DataRequest = AF.request(encodedRequest)
        alamofireRequest.responseJSON { [weak self] response in
            switch response.result {
            case .success:
                /*if let data = response.data {
                 let string = String(data: data, encoding: .utf8)
                 print("performRequest response \(string)")
                 }*/
                self?.parseResponse(data: response.data, responseModel: responseModel, completion: completion)
            case .failure(let error):
                completion(.error(message: error.localizedDescription))
            }
        }
    }
}

// MARK: - Private Functions
extension RequestManager {
    private func returnDataFromMockup<T: Decodable>(mockDataFilename: String, responseModel: T.Type, completion: @escaping (RequestManagerResponse) -> Void) {
        if let path = Bundle.main.path(forResource: mockDataFilename, ofType: "json") {
            if let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe) {
                parseResponse(data: data, responseModel: responseModel, completion: completion)
                return
            }
        }
        completion(.error(message: "Error on parse data from bundle"))
    }
    
    private func parseResponse<T: Decodable>(data: Data?, responseModel: T.Type, completion: @escaping (RequestManagerResponse) -> Void) {
        guard let jsonData = data else {
            completion(.error(message: "Parse JSON error"))
            return
        }
        /*
         if let jsonResult = try? jsonDecoder.decode(responseModel, from: jsonData) {
         completion(.success(model: jsonResult as Any))
         return
         }
         */
        var errorStr = "Parse JSON error"
        if let jsonResult = try? jsonDecoder.decode(responseModel, from: jsonData) {
            completion(.success(model: jsonResult as Any))
            return
        } else { //print decode error for debug purposes
            do {
                _ = try jsonDecoder.decode(responseModel, from: jsonData)
            } catch let error {
                errorStr = error.localizedDescription
                
                if let decError = error as? DecodingError, let debugInfo = decError.localizedDebugInfo {
                    errorStr = debugInfo
                }
                print("parseResponse \(errorStr)")
            }
        }
        
        if let dict = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
            if let message = dict["error"] as? String {
                completion(.error(message: message))
                return
            }
        }
        completion(.error(message: "Parse JSON error"))
    }
}

extension DecodingError {
    public var localizedDebugInfo: String? {
        switch self {
        case .dataCorrupted(let context):
            return NSLocalizedString(context.debugDescription, comment: "")
        case .keyNotFound(_, let context):
            return NSLocalizedString("\(context.debugDescription)", comment: "")
        case .typeMismatch(_, let context):
            return NSLocalizedString("\(context.debugDescription)", comment: "")
        case .valueNotFound(_, let context):
            return NSLocalizedString("\(context.debugDescription)", comment: "")
        @unknown default:
            return NSLocalizedString("Something went wrong...", comment: "")
        }
    }
}
