//
//  ResponseCodes.swift
//  Newsdrop
//
//  Created by Serhii Kobzin on 13.02.2020.
//  Copyright © 2020 Onix-Systems. All rights reserved.
//

enum ResponseCodes: String {
    case bookmarksLimitExceeded = "limit_exceeded"
    case noInternet = "no_internet"
}
