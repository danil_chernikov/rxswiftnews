//
//  GetHeadlinesNewsRequest.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import Alamofire

class GetHeadlinesNewsRequest: RequestProtocol {
    private let locale: String
    
    init(locale: String) {
        self.locale = locale
    }
    
    var mockDataFilename: String? {
        return nil
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var endpoint: String {
        return "\(Defines.baseURL)/top-headlines?country=\(locale)&apiKey=\(Defines.keyAPI)"
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var parameters: [String : Any]? {
        return nil
    }
    
    var rawData: Data? {
        return nil
    }
    
    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
}
