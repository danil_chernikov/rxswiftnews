//
//  Request.swift
//  Newsdrop
//
//  Created by Serhii Kobzin on 05.02.2020.
//  Copyright © 2020 Onix-Systems. All rights reserved.
//

import Alamofire

enum RequestResponse {
    case success(model: Any?)
    case error(message: String)
}

protocol RequestProtocol: AnyObject {
    var mockDataFilename: String? { get }
    var httpMethod: HTTPMethod { get }
    var endpoint: String { get }
    var headers: [String: String]? { get }
    var parameters: [String: Any]? { get }
    var rawData: Data? { get }
    var encoding: ParameterEncoding { get }
}

extension RequestProtocol {
    func performRequest<T: Decodable>(responseModel: T.Type, completion: @escaping (RequestResponse) -> Void) {
            RequestManager.shared.performRequest(self, responseModel: responseModel, completion: { response in
                switch response {
                case .success(let model):
                    completion(.success(model: model))
                case .error(let message):
                    completion(.error(message: message))
                }
            }
        )
    }
}
