//
//  NewsSourceCell.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit

class NewsSourceCell: UICollectionViewCell {
    @IBOutlet private weak var titleLabel: UILabel!

    func setup(title: String) {
        self.titleLabel.text = title
    }
}
