//
//  NewsCell.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    @IBOutlet private weak var backView: UIView!
    @IBOutlet private weak var articleImageView: UIImageView!
    @IBOutlet private weak var articleTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backView.layer.cornerRadius = 12
        self.backView.layer.borderWidth = 0.2
        self.backView.layer.borderColor = UIColor.black.withAlphaComponent(0.30).cgColor
    }
    
    func setup(articleModel: ArticleModel) {
        self.articleTitleLabel.text = articleModel.title
        self.articleImageView.load(from: URL(string: articleModel.imageURL), defaultImage: nil)
    }
}
