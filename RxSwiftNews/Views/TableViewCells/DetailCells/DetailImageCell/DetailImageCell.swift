//
//  DetailImageCell.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 22.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit

class DetailImageCell: UITableViewCell {
    @IBOutlet private weak var articleImageView: UIImageView!
    
    func setuImage(url: URL?) {
        self.articleImageView.load(from: url, defaultImage: nil)
        self.articleImageView.layer.cornerRadius = 12
    }
}
