//
//  DetailDescriptionCell.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 22.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit

class DetailDescriptionCell: UITableViewCell {
    @IBOutlet private weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.descriptionLabel.font = UIFont.Helvetica.regular(size: 17)
        self.descriptionLabel.textColor = UIColor(hex: 0x000000)
    }
    
    func setupDescription(text: String) {
        self.descriptionLabel.text = text
    }
}
