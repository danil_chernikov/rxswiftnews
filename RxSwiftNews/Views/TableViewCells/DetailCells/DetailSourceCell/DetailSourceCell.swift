//
//  DetailSourceCell.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 03.08.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit
import RxSwift

class DetailSourceCell: UITableViewCell {
    @IBOutlet private weak var sourceButton: UIButton!
    
    private let disposeBag = DisposeBag()
    
    var didTapBtnObservable: Observable<Void> {
        return sourceButton.rx.tap.asObservable()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let titleAttribute: NSAttributedString = .init(string: "read more".uppercased(), attributes: [NSAttributedString.Key.font: UIFont.Helvetica.bold(size: 20) ?? UIFont(),
                                                                                                      NSAttributedString.Key.foregroundColor: UIColor.white])
        self.sourceButton.setAttributedTitle(titleAttribute, for: .normal)
        self.sourceButton.backgroundColor = UIColor(hex: 0xED526A)
        self.sourceButton.layer.cornerRadius = 12
    }
}
