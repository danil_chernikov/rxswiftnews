//
//  DetailTitleCell.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 22.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit

class DetailTitleCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.font = UIFont.Helvetica.regular(size: 20)
        self.titleLabel.textColor = UIColor(hex: 0x000000)
    }
    
    func setupTitle(text: String) {
        self.titleLabel.text = text
    }
}
