//
//  BreakingNewsCell.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit

class BreakingNewsCell: UITableViewCell {
    @IBOutlet private weak var backView: UIView!
    @IBOutlet private weak var newsImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        self.backView.layer.cornerRadius = self.backView.bounds.height / 2
    }
    
    func setup(title: String, newsImage: UIImage?) {
        self.titleLabel.text = title
        self.newsImageView.image = newsImage
    }
}
