//
//  BrowserTopBar.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 04.08.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//


import WebKit
import RxSwift
import RxCocoa
import RxWebKit

class BrowserTopBar: UIView {
    @IBOutlet private weak var closeBtn: UIButton!
    @IBOutlet private weak var backBtn: UIButton!
    @IBOutlet private weak var linkFieldLbl: UILabel!
    @IBOutlet private weak var forwardBtn: UIButton!
    @IBOutlet private weak var menuBtn: UIButton!
    
    private weak var webView: WKWebView?
    private let disposeBag = DisposeBag()
    
    var closeBtnObservable: Observable<Void> {
        return closeBtn.rx.tap.asObservable()
    }
    
    var menuBtnObservable: Observable<Void> {
        return menuBtn.rx.tap.asObservable()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        nibSetup()
    }
}

extension BrowserTopBar {
    func setupBar(webView: WKWebView) {
        self.webView = webView
        
        self.bindRx()
    }
    
    private func bindRx() {
        guard let webView = self.webView else { return }
        webView.rx.url
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.linkFieldLbl.text = $0?.absoluteString
            }).disposed(by: disposeBag)
        
        webView.rx.canGoBack
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.backBtn.isEnabled = $0
            }).disposed(by: disposeBag)
        
        webView.rx.canGoForward
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.forwardBtn.isEnabled = $0
            }).disposed(by: disposeBag)
        
        self.backBtn.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.webView?.goBack()
            }).disposed(by: disposeBag)
        
        self.forwardBtn.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.webView?.goForward()
            }).disposed(by: disposeBag)
    }
}
