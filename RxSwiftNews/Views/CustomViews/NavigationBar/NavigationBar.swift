//
//  NavigationBar.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 04.08.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NavigationBar: UIView {
    struct NavigationBarItem {
        let title: String?
        let image: UIImage?
        
        init(title: String? = nil, image: UIImage? = nil) {
            self.title = title
            self.image = image
        }
    }
    
    @IBOutlet private weak var leftButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var rightButton: UIButton!
    
    var leftButtonObservable: Observable<Void> {
        return leftButton.rx.tap.asObservable()
    }
    
    var rightButtonObservable: Observable<Void> {
        return rightButton.rx.tap.asObservable()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        nibSetup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        leftButton.isHidden = true
        titleLabel.isHidden = true
        rightButton.isHidden = true
        
        titleLabel.font = UIFont.Helvetica.regular(size: 16)
    }
    
    func setupBar(title: String? = nil, leftItem: NavigationBarItem? = nil, rightItem: NavigationBarItem? = nil) {
        self.titleLabel.text = title
        self.titleLabel.isHidden = title == nil
        
        self.leftButton.setTitle(leftItem?.title, for: .normal)
        self.leftButton.setImage(leftItem?.image, for: .normal)
        self.leftButton.isHidden = leftItem == nil || leftItem?.title == nil && leftItem?.image == nil
        
        self.rightButton.setTitle(rightItem?.title, for: .normal)
        self.rightButton.setImage(rightItem?.image, for: .normal)
        self.rightButton.isHidden = rightItem == nil || rightItem?.title == nil && rightItem?.image == nil
    }
}
