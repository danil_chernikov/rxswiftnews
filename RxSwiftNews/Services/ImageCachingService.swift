//
//  ImageCachingService.swift
//  Newsdrop
//
//  Created by Danil Chernikov on 20.07.2020.
//  Copyright © 2020 Onix-Systems. All rights reserved.

import Foundation
import UIKit
import CommonCrypto
import Dispatch

private let kConcurentRequestsCount: Int = 5

class ImageCacheItem {
    let url: URL
    var bInProgress: Bool = false
    
    init(url: URL) {
        self.url = url
    }
}

protocol ImageCachingServiceType: Service {
    func loadImages(items: [URL])
    func load(from imageURL: URL?, completion: ((UIImage?) -> Void)?)
    func cleanCache()
}

class ImageCachingService: ImageCachingServiceType {
    static let shared: ImageCachingServiceType = ImageCachingService()
    private let myPropertyQueue = DispatchQueue(label: "thread_safe", qos: .background, attributes: .concurrent, autoreleaseFrequency: .never)
    private var itemsQueue: [ImageCacheItem] = []
    private var pathForFolder: String?
    
    init() {
        createPathForFolder()

        //debug only
        //cleanCache()
    }
    
    func load(from imageURL: URL?, completion: ((UIImage?) -> Void)? = nil) {
        guard let url = imageURL else {
            completion?(nil)
            return
        }
        
        //print("ImageCachingService load \(url)")

        //check for cache
        if let image = loadImageFromCache(url: url.absoluteString) {
            completion?(image)
            return
        }
        
        let request = URLRequest(url: url)
        DispatchQueue.global(qos: .userInitiated).async {
            let session = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] data, response, _ in
                if let data = data, let response = response, ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300, let image = UIImage(data: data) {
                    self?.saveImageToCache(url: url.absoluteString, imageData: data)
                    completion?(image)
                }}
            )
            session.resume()
        }
    }
    
    private func saveImageToCache(url: String, imageData: Data) {
        guard let filePath = getPath(urlString: url) else { return }
        let filePathUrl = URL(fileURLWithPath: filePath)
        try? imageData.write(to: filePathUrl)
        //print("ImageCachingService saveImageToCache \(url)")
    }
    
    private func loadImageFromCache(url: String) -> UIImage? {
        guard let filePath = getPath(urlString: url) else { return nil }
        if checkFromImageFromCache(url: url) {
            if let image = UIImage(contentsOfFile: filePath) {
                //print("ImageCachingService from cache \(url)")
                return image
            }
        }
        return nil
    }
    
    private func checkFromImageFromCache(url: String) -> Bool {
        guard let filePath = getPath(urlString: url) else { return false }
        if FileManager.default.fileExists(atPath: filePath) {
            return true
        }
        return false
    }
}

// MARK: - Prefetch images
extension ImageCachingService {
    func loadImages(items: [URL]) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.loadImagesThread(items: items)
        }
    }
    
    private func loadImagesThread(items: [URL]) {
        for item in items {
            //check for already in queue
            let filtered = self.itemsQueue.filter({ $0.url == item })
            //check for already exist
            let exist = checkFromImageFromCache(url: item.absoluteString)
            if filtered.isEmpty, !exist {
                _ = myPropertyQueue.sync { [weak self] in
                    self?.itemsQueue.append(ImageCacheItem(url: item))
                }
            }
        }
        
        let inProgress = itemsQueue.filter( { $0.bInProgress == true })
        for _ in inProgress.count..<kConcurentRequestsCount {
            prefetchImages()
        }
    }
    
    private func prefetchImages() {
        _ = myPropertyQueue.sync { [weak self] in
            let inProgress = self?.itemsQueue.filter( { $0.bInProgress == true }) ?? []
            let filtered = self?.itemsQueue.filter( { $0.bInProgress == false }) ?? []
            if inProgress.count <= kConcurentRequestsCount, let first = filtered.first {
                //print("prefetchImages in progress \(first.url.absoluteString), in progress \(inProgress.count), queued \(filtered.count)")
                first.bInProgress = true
                prefetchImage(item: first)
            }
        }
    }
    
    private func prefetchImage(item: ImageCacheItem) {
        load(from: item.url) { [weak self] _ in
            self?.removeItemFromQueue(url: item.url)
            self?.prefetchImages()
        }
    }
    
    private func removeItemFromQueue(url: URL) {
        _ = myPropertyQueue.sync { [weak self] in
            if let index = self?.itemsQueue.firstIndex(where: { $0.url == url }) {
                self?.itemsQueue.remove(at: index)
                //print("prefetchImages remove \(url.absoluteString)")
            }
        }
    }
}

// MARK: - Disk routine
extension ImageCachingService {
    func cleanCache() {
        guard let folder = pathForFolder else { return }
        let folderURL = URL(fileURLWithPath: folder)
        try? FileManager.default.removeItem(at: folderURL)
        
        createPathForFolder()
    }
    
    private func createPathForFolder() {
        guard let cacheDirPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first else { return }
        let folderPath = cacheDirPath + "/ImageViewCache"
        
        if FileManager.default.fileExists(atPath: folderPath) == false {
            do {
                try FileManager.default.createDirectory(atPath: folderPath, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        pathForFolder = folderPath
    }
    
    private func getPath(urlString: String) -> String? {
        // added url contain runtime query items, cache dont work
        guard let url = URL(string: urlString), let host = url.host else { return nil }
        guard let path = pathForFolder else { return nil }
        let file = MD5(string: host + url.path)
        let fileUrl = path + "/" + file + ".jpg"
        return fileUrl
    }
    
    private func MD5(string: String) -> String {
        let data = string.data(using: .utf8) ?? Data()
        let hash = data.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) -> [UInt8] in
            var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            CC_MD5(bytes.baseAddress, CC_LONG(data.count), &hash)
            return hash
        }
        let md5Hex = hash.map { String(format: "%02x", $0) }.joined()
        return md5Hex
    }
}
