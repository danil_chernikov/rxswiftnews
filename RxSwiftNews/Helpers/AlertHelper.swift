//
//  AlertHelper.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 04.08.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AlertHelper {
    struct AlertAction {
        let title: String?
        let style: UIAlertAction.Style
        let action: PublishSubject<Void>?
        
        static func action(title: String? = nil, style: UIAlertAction.Style = .default, action: PublishSubject<Void>? = nil) -> AlertAction {
            return AlertAction(title: title, style: style, action: action)
        }
    }
    
    static func present(parentViewController: UIViewController, title: String? = nil, message: String? = nil, style: UIAlertController.Style, actions: [AlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        
        actions.forEach { action in
            let alertAction = UIAlertAction(title: action.title, style: action.style) { _ in
                action.action?.asObserver().onNext(())
                action.action?.asObserver().onCompleted()
            }
            alertController.addAction(alertAction)
        }
        parentViewController.present(alertController, animated: true)
    }
}
