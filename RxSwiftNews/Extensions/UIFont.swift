//
//  UIFont.swift
//  EnglishCoach
//
//  Created by Danil Chernikov on 25.06.2020.
//  Copyright © 2020 onix. All rights reserved.
//

import UIKit

extension UIFont {
    class func fontList() {
        for family in UIFont.familyNames.sorted() {
            let names = UIFont.fontNames(forFamilyName: family)
            print("Family: \(family) Font names: \(names)")
        }
    }
    
    final class Helvetica {
        static func regular(size: CGFloat) -> UIFont? {
            return UIFont(name: "Helvetica", size: size)
        }
        
        static func bold(size: CGFloat) -> UIFont? {
            return UIFont(name: "Helvetica-Bold", size: size)
        }
    }
    
    final class Ubuntu {
        static func bold(size: CGFloat) -> UIFont? {
            return UIFont(name: "Ubuntu-Bold", size: size)
        }
    }
    
    final class HelveticaNeue {
        static func regular(size: CGFloat) -> UIFont? {
            return UIFont(name: "HelveticaNeue", size: size)
        }
    }
}

/*
 Family: Helvetica Font names: ["Helvetica-Oblique", "Helvetica-BoldOblique", "Helvetica", "Helvetica-Light", "Helvetica-Bold", "Helvetica-LightOblique"]
 
 Family: Helvetica Neue Font names: ["HelveticaNeue-UltraLightItalic", "HelveticaNeue-Medium", "HelveticaNeue-MediumItalic", "HelveticaNeue-UltraLight", "HelveticaNeue-Italic", "HelveticaNeue-Light", "HelveticaNeue-ThinItalic", "HelveticaNeue-LightItalic", "HelveticaNeue-Bold", "HelveticaNeue-Thin", "HelveticaNeue-CondensedBlack", "HelveticaNeue", "HelveticaNeue-CondensedBold", "HelveticaNeue-BoldItalic"]
 */
