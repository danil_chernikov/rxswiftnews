//
//  UIViewController+StatusBar.swift
//  EnglishCoach
//
//  Created by Альона Дробко on 22.01.2020.
//  Copyright © 2020 onix. All rights reserved.
//

import UIKit

class ChangeStyleBarExtension: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.statusBarStyle
    }
    var statusBarStyle: UIStatusBarStyle = .default
    
    func setUpStyleBar() {
        if #available(iOS 13.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                self.statusBarStyle = .darkContent
            } else {
                self.statusBarStyle = .lightContent
            }
        }
        setNeedsStatusBarAppearanceUpdate()
    }
}
