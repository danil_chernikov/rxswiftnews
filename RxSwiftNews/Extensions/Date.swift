//
//  Date.swift
//  EnglishCoach
//
//  Created by Sergey Pritula on 23.10.2019.
//  Copyright © 2019 onix. All rights reserved.
//

import Foundation

private let formatter = DateFormatter()

extension Date {
    func dateString(with format: String, locale: Locale? = nil) -> String {
        formatter.dateFormat = format
        if let locale = locale {
            formatter.locale = locale
        }
        let dateString = formatter.string(from: self)
        return dateString
    }
    
    static let iso8601Full: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
      formatter.calendar = Calendar(identifier: .iso8601)
      formatter.timeZone = TimeZone(secondsFromGMT: 0)
      formatter.locale = Locale(identifier: "en_US_POSIX")
      return formatter
    }()
}
