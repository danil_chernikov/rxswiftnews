//
//  Double.swift
//  EnglishCoach
//
//  Created by Sergey Pritula on 23.10.2019.
//  Copyright © 2019 onix. All rights reserved.
//

import UIKit

extension Double {
    var degreesToRadians: CGFloat { return CGFloat(self * .pi / 180) }
    var radiansToDegrees: CGFloat { return CGFloat(self * 180 / .pi) }
}

extension Double {
    /// Rounds the double to decimal places value
    mutating func roundToPlaces(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        let temp = self * divisor
        
        return Darwin.round(temp) / divisor
    }
}
