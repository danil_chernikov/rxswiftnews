//
//  String+AddAttributes.swift
//  EnglishCoach
//
//  Created by Sergey Pritula on 23.10.2019.
//  Copyright © 2019 onix. All rights reserved.
//

import UIKit

extension String {

    func nsRange(of toFind: String) -> NSRange? {
        if let range = self.range(of: toFind) {
            return NSRange(range, in: self)
        }
        return nil
    }
    
    func addAttributes(_ attributes: [NSAttributedString.Key: Any]? = nil,
                       startMark: String? = nil,
                       endMark: String? = nil,
                       substringAttributes: [NSAttributedString.Key: Any]? = nil) -> NSMutableAttributedString {
        let string = self
        
        let mutableAttributedString = NSMutableAttributedString(string: string, attributes: attributes)
        
        guard let startMark = startMark, let endMark = endMark, let substringAttributes = substringAttributes else {
            return mutableAttributedString
        }
        
        let startRanges = string.allRanges(of: startMark)
        let endRanges = string.allRanges(of: endMark)
        
        var offset = 0
        
        for i in 0..<startRanges.count {
            let startRange = NSRange(location: startRanges[i].lowerBound - offset, length: startMark.count)
            var endRange = NSRange(location: endRanges[i].lowerBound - offset, length: endMark.count)
            let nsRange = NSRange(location: startRange.upperBound, length: endRange.lowerBound - startRange.upperBound)
            
            mutableAttributedString.addAttributes(substringAttributes, range: nsRange)
            
            mutableAttributedString.replaceCharacters(in: startRange, with: NSAttributedString(string: ""))
            
            endRange = NSRange(location: endRange.lowerBound - startMark.count, length: endMark.count)
            mutableAttributedString.replaceCharacters(in: endRange, with: NSAttributedString(string: ""))
            
            offset += startMark.count + endMark.count
        }
        
        return mutableAttributedString
    }
    
    func superscriptString(with font: UIFont, startMark: String, endMark: String) -> NSAttributedString? {
        guard let fontSuper = UIFont(name: font.familyName, size: font.pointSize) else { return nil }
        
        let attributedTitle = self.addAttributes(
            nil,
            startMark: startMark,
            endMark: endMark,
            substringAttributes: [.font: fontSuper, .baselineOffset: font.pointSize / 2])
        
        return attributedTitle
    }
    
    func encodeUrl() -> String? {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    
    func decodeUrl() -> String? {
        return self.removingPercentEncoding
    }
    
}

extension String {
    func allRangesHelp(of aString: String,
                       options: String.CompareOptions = [],
                       range: Range<String.Index>? = nil,
                       locale: Locale? = nil) -> [Range<String.Index>] {
        
        //the slice within which to search
        var ranges = [Range<String.Index>]()
        
        if let range = range {
            let slice = String(self[range])
            
            var previousEnd = self.startIndex
            
            while let r = slice.range(of: aString, options: options,
                                      range: previousEnd ..< self.endIndex,
                                      locale: locale) {
                                        if previousEnd != self.endIndex { //don't increment past the end
                                            previousEnd = self.index(after: r.lowerBound)
                                        }
                                        ranges.append(r)
            }
            
        } else {
            let slice = self
            
            var previousEnd = self.startIndex
            
            while let r = slice.range(of: aString, options: options,
                                      range: previousEnd ..< self.endIndex,
                                      locale: locale) {
                                        if previousEnd != self.endIndex { //don't increment past the end
                                            previousEnd = self.index(after: r.lowerBound)
                                        }
                                        ranges.append(r)
            }
        }
        
        return ranges
    }
    
    func allRanges(of aString: String,
                   options: String.CompareOptions = [],
                   range: Range<String.Index>? = nil,
                   locale: Locale? = nil) -> [Range<Int>] {
        return allRangesHelp(of: aString, options: options, range: range, locale: locale)
            .map(indexRangeToIntRange)
    }
    
    func indexToInt(_ index: String.Index) -> Int {
        return self.distance(from: self.startIndex, to: index)
    }
    
    func indexRangeToIntRange(_ range: Range<String.Index>) -> Range<Int> {
        return indexToInt(range.lowerBound) ..< indexToInt(range.upperBound)
    }
}

extension NSMutableAttributedString {
    
    func addAttributes(startMark: String,
                       endMark: String,
                       substringAttributes: [NSAttributedString.Key: Any]? = nil) {
        guard let substringAttributes = substringAttributes else { return }
        
        let startRanges = self.string.allRanges(of: startMark)
        let endRanges = self.string.allRanges(of: endMark)
        
        var offset = 0
        
        for i in 0..<startRanges.count {
            let startRange = NSRange(location: startRanges[i].lowerBound - offset, length: startMark.count)
            var endRange = NSRange(location: endRanges[i].lowerBound - offset, length: endMark.count)
            let nsRange = NSRange(location: startRange.upperBound, length: endRange.lowerBound - startRange.upperBound)
            
            self.addAttributes(substringAttributes, range: nsRange)
            
            self.replaceCharacters(in: startRange, with: NSAttributedString(string: ""))
            
            endRange = NSRange(location: endRange.lowerBound - startMark.count, length: endMark.count)
            self.replaceCharacters(in: endRange, with: NSAttributedString(string: ""))
            
            offset += startMark.count + endMark.count
        }
    }
    
}
