//
//  UIImageViewExtensions.swift
//  Newsdrop
//
//  Created by Serhii Kobzin on 10.02.2020.
//  Copyright © 2020 Onix-Systems. All rights reserved.
//

import UIKit

extension UIImageView {
    func load(from imageURL: URL?, defaultImage: UIImage?, completion: (() -> Void)? = nil) {
        self.image = nil
        guard let url = imageURL else { return }
        ImageCachingService.shared.load(from: url) { [weak self] image in
            self?.transition(toImage: image ?? defaultImage)
        }
    }
    
    private func transition(toImage image: UIImage?) {
        if Thread.isMainThread {
            self.image = image
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.image = image
            }
        }
    }
}
