//
//  UITableViewCell.swift
//  EnglishCoach
//
//  Created by Sergey Pritula on 23.10.2019.
//  Copyright © 2019 onix. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var reuseIdentifier: String { return String(describing: self) }
}

extension UICollectionViewCell {
    static var identifier: String {
        return String(describing: self)
    }
    
}

protocol NibLoadableView: class {
    static var nibName: String { get }
}

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}

extension UITableViewCell: NibLoadableView {}
extension UICollectionViewCell: NibLoadableView {}

extension UITableView {
    func registerNib<T: UITableViewCell>(cellType: T.Type) {
        self.register(UINib(nibName: T.nibName, bundle: nil), forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeCell<T: UITableViewCell>(for indexPath: IndexPath) -> T? {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.nibName, for: indexPath) as? T else { return nil }
        return cell
    }

}

extension UICollectionView {
    func registerNib<T: UICollectionViewCell>(_ cellType: T.Type...) {
        cellType.forEach { [weak self] in
            guard let self = self else { return }
            self.register(UINib(nibName: $0.nibName, bundle: nil), forCellWithReuseIdentifier: $0.nibName)
        }
    }
    
    func dequeCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T? {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.nibName, for: indexPath) as? T else { return nil }
        return cell
    }
    
}

extension UITableView {
    
    func isLast(for indexPath: IndexPath) -> Bool {
        let indexOfLastSection = indexPath.section
        let indexOfLastRowInLastSection = numberOfRows(inSection: indexOfLastSection) - 1
        
        return indexPath.section == indexOfLastSection && indexPath.row == indexOfLastRowInLastSection
    }
}

extension UICollectionView {
    
    func isLast(for indexPath: IndexPath) -> Bool {
        let indexOfLastSection = numberOfSections > 0 ? numberOfSections - 1 : 0
        let indexOfLastRowInLastSection = numberOfItems(inSection: indexOfLastSection) - 1
        
        return indexPath.section == indexOfLastSection && indexPath.row == indexOfLastRowInLastSection
    }
}
