//
//  UITextView.swift
//  EnglishCoach
//
//  Created by Альона Дробко on 22.06.2020.
//  Copyright © 2020 onix. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    func decreaseFontSize() {
        self.font = UIFont(name: (self.font?.fontName) ?? "Helvetica", size: (self.font?.pointSize ?? 0) - 1)
    }
    
    func increaseFontSize () {
        self.font = UIFont(name: (self.font?.fontName) ?? "Helvetica", size: (self.font?.pointSize ?? 0) + 1)
    }
}

// MARK: - UITextView extension
extension UITextView {
    
    func centerVerticalText() {
        self.textAlignment = .center
        let fitSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fitSize)
        let calculate = (bounds.size.height - size.height * zoomScale) / 2
        let offset = max(1, calculate)
        contentOffset.y = -offset
    }
}

extension UITextView {
    
    func adjustFontToFitText(minimumScale: CGFloat, height: CGFloat, width: CGFloat) {
        guard let font = font else { return }
        
        let scale = max(0.0, min(1.0, minimumScale))
        let minimumFontSize = font.pointSize * scale
        adjustFontToFitText(minimumFontSize: minimumFontSize, height: height, width: width)
    }
    
    func adjustFontToFitText(minimumFontSize: CGFloat, height: CGFloat, width: CGFloat) {
        guard let font = font, minimumFontSize > 0.0 else { return }
        
        let minimumSize = floor(minimumFontSize)
        var fontSize = font.pointSize
        let text = self.text
        var heightText: CGFloat = text?.height(withConstrainedWidth: width, font: self.font ?? UIFont()) ?? 0
        
        while heightText >= height {
            guard fontSize > minimumSize else { break }
            
            fontSize -= 1.0
            let newFont = font.withSize(fontSize)
            heightText = text?.height(withConstrainedWidth: width, font: newFont) ?? 0
        }
        
        while heightText < height {
            guard fontSize > 35 else { break }
            
            fontSize += 1.0
            let newFont = font.withSize(fontSize)
            heightText = text?.height(withConstrainedWidth: width, font: newFont) ?? 0
        }
        self.font = font.withSize(fontSize)
    }
}
