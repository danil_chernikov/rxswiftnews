//
//  UIStoryboard.swift
//  EnglishCoach
//
//  Created by Sergey Pritula on 23.10.2019.
//  Copyright © 2019 onix. All rights reserved.
//

import UIKit

struct Storyboard {
}

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self.self)
    }
}

extension UIStoryboard {
    func instantiate<T: StoryboardIdentifiable>() -> T {
        guard let vc = instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Controller is nil with the identifier: \(T.storyboardIdentifier)")
        }
        return vc
    }
}

extension UIViewController: StoryboardIdentifiable {}
