//
//  Optional.swift
//  EnglishCoach
//
//  Created by Sergey Pritula on 23.10.2019.
//  Copyright © 2019 onix. All rights reserved.
//

import Foundation

public extension Optional {
    func `do`(_ action: (Wrapped) -> Void) {
        self.map(action)
    }
}
