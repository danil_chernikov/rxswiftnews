//
//  DetailBlockModel.swift
//  RxSwiftNews
//
//  Created by Danil Chernikov on 22.07.2020.
//  Copyright © 2020 Danil Chernikov. All rights reserved.
//

import Foundation

struct DetailBlockModel {
    enum BlockType {
        case image(URL?)
        case title(String)
        case description(String)
        case source
    }
    
    let type: BlockType
}
